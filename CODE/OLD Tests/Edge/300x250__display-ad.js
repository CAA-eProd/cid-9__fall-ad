(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.webFontTxtInst = {}; 
var loadedTypekitCount = 0;
var loadedGoogleCount = 0;
var gFontsUpdateCacheList = [];
var tFontsUpdateCacheList = [];
lib.ssMetadata = [
		{name:"300x250__display_ad_atlas_", frames: [[111,117,115,34],[114,0,111,64],[0,0,112,64],[0,160,83,36],[111,153,112,30],[127,66,111,49],[0,117,109,41],[0,66,125,49]]}
];



lib.updateListCache = function (cacheList) {		
	for(var i = 0; i < cacheList.length; i++) {		
		if(cacheList[i].cacheCanvas)		
			cacheList[i].updateCache();		
	}		
};		

lib.addElementsToCache = function (textInst, cacheList) {		
	var cur = textInst;		
	while(cur != null && cur != exportRoot) {		
		if(cacheList.indexOf(cur) != -1)		
			break;		
		cur = cur.parent;		
	}		
	if(cur != exportRoot) {		
		var cur2 = textInst;		
		var index = cacheList.indexOf(cur);		
		while(cur2 != null && cur2 != cur) {		
			cacheList.splice(index, 0, cur2);		
			cur2 = cur2.parent;		
			index++;		
		}		
	}		
	else {		
		cur = textInst;		
		while(cur != null && cur != exportRoot) {		
			cacheList.push(cur);		
			cur = cur.parent;		
		}		
	}		
};		

lib.gfontAvailable = function(family, totalGoogleCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], gFontsUpdateCacheList);		

	loadedGoogleCount++;		
	if(loadedGoogleCount == totalGoogleCount) {		
		lib.updateListCache(gFontsUpdateCacheList);		
	}		
};		

lib.tfontAvailable = function(family, totalTypekitCount) {		
	lib.properties.webfonts[family] = true;		
	var txtInst = lib.webFontTxtInst && lib.webFontTxtInst[family] || [];		
	for(var f = 0; f < txtInst.length; ++f)		
		lib.addElementsToCache(txtInst[f], tFontsUpdateCacheList);		

	loadedTypekitCount++;		
	if(loadedTypekitCount == totalTypekitCount) {		
		lib.updateListCache(tFontsUpdateCacheList);		
	}		
};
// symbols:



(lib.Bitmap3 = function() {
	this.spriteSheet = ss["300x250__display_ad_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.burger_0000_buntop = function() {
	this.spriteSheet = ss["300x250__display_ad_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.burger_0001_lettuce = function() {
	this.spriteSheet = ss["300x250__display_ad_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.burger_0002_cucumbers = function() {
	this.spriteSheet = ss["300x250__display_ad_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.burger_0003_bacon = function() {
	this.spriteSheet = ss["300x250__display_ad_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.burger_0004_beef = function() {
	this.spriteSheet = ss["300x250__display_ad_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.burger_0005_bunbottom = function() {
	this.spriteSheet = ss["300x250__display_ad_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.insurancelogo = function() {
	this.spriteSheet = ss["300x250__display_ad_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.txtslogan = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#00338D").s().p("AgIAtIgJgCQgIgEgHgGIgFgHIgFgIIgCgIIgBgKQAAgJADgIIAFgIIAFgHQAHgGAIgEQAIgDAJAAQAJAAAJADQAIAEAHAGQAGAGAEAJQADAIAAAJIgBAKIgCAIQgEAJgGAGQgHAGgIAEIgJACIgJABIgIgBgAgHglIgHACQgHADgFAFQgGAFgCAHQgDAIAAAHQAAAIADAIIADAGIAFAGQAFAGAHADIAHACIAHABIAIgBIAHgCQAHgDAFgGIAFgGIADgGIACgIIABgIIgBgHIgCgIQgDgHgFgFQgFgFgHgDQgHgDgIAAIgHABgAAOAbIgOgXIgJAAIAAAXIgIAAIAAg1IAUAAIAIABIAGADQAFAEAAAIQAAAHgFADIgEACIgFABIAPAYgAgJgCIAJAAIAFAAIAEgBIADgDQACgCAAgDIgCgEIgDgDIgDgBIgFAAIgKAAg");
	this.shape.setTransform(247.1,22.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00338D").s().p("AgLAMIAAgXIAXAAIAAAXg");
	this.shape_1.setTransform(235,32.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#00338D").s().p("AgdA6IAAhxIASAAIAAAYIAAAAIAHgMQAEgFAEgDQALgHAPABIAAAUQgLAAgJADQgHADgEAGQgFAGgCAIQgCAJAAAKIAAAyg");
	this.shape_2.setTransform(229,27.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#00338D").s().p("AgVA3QgKgEgHgJQgHgHgDgLQgDgLgBgNQAAgMAEgLQAEgKAHgIQAHgJAKgEQAKgEALAAQAHgBAHACIALAFQAKAGAGAKQAGAKACALQADALgBAKIhUAAQAAAHACAHQACAHAEAFQAFAFAGADQAHADAIAAQAMAAAIgFQAEgDACgEQADgEABgGIASAAQgDATgNAJQgHAFgIADQgIADgJgBQgNAAgKgEgAgNgnQgFACgFAFQgEAEgCAGQgDAFAAAHIBAAAQAAgHgDgFQgCgHgEgDQgFgFgGgCQgFgDgHAAQgHAAgGADg");
	this.shape_3.setTransform(218.9,27.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#00338D").s().p("AAQBKIgMgBQgEgBgEgDQgDgDgBgEQgCgFAAgIIAAhHIgTAAIAAgRIATAAIAAgiIASAAIAAAiIAWAAIAAARIgWAAIAABGIAAAEIACAEIAFABIAHABIAIAAIAAAQg");
	this.shape_4.setTransform(209.2,26.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#00338D").s().p("AAQBKIgMgBQgEgBgEgDQgDgDgBgEQgCgFAAgIIAAhHIgTAAIAAgRIATAAIAAgiIASAAIAAAiIAWAAIAAARIgWAAIAABGIAAAEIACAEIAFABIAHABIAIAAIAAAQg");
	this.shape_5.setTransform(202.3,26.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#00338D").s().p("AgVA3QgKgEgHgJQgHgHgDgLQgDgLgBgNQAAgMAEgLQAEgKAHgIQAHgJAKgEQAKgEALAAQAHgBAHACIALAFQAKAGAGAKQAGAKACALQADALgBAKIhUAAQAAAHACAHQACAHAEAFQAFAFAGADQAHADAIAAQAMAAAIgFQAEgDACgEQADgEABgGIASAAQgDATgNAJQgHAFgIADQgIADgJgBQgNAAgKgEgAgNgnQgFACgFAFQgEAEgCAGQgDAFAAAHIBAAAQAAgHgDgFQgCgHgEgDQgFgFgGgCQgFgDgHAAQgHAAgGADg");
	this.shape_6.setTransform(193.3,27.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#00338D").s().p("AgGBPIgKgDIgKgFQgEgEgDgFIgBAAIAAAPIgTAAIAAicIATAAIAAA7IABAAQABgFAFgDQADgEAFgCQAKgEALAAIAMABQAGABAEADQAKAFAHAIQAGAIADALQAEAKAAAMQAAAMgDALQgEAKgGAIQgHAIgJAFQgKAFgNAAgAgQgSQgHAEgEAGQgEAGgCAHQgCAHABAIQAAAJACAHQABAIAFAGQAEAGAHAEQAHADAIAAQAKAAAHgDQAGgEAEgGQAEgGABgIQACgIAAgJQAAgHgCgIQgCgHgEgGQgFgGgGgDQgHgEgIAAQgKAAgGAEg");
	this.shape_7.setTransform(181.1,25.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#00338D").s().p("AgRA6QgJgCgGgFQgHgFgEgHQgEgHAAgLIASAAQABAGACAEQADAFAEADQAEACAGABIAKABIAJgBIAJgCQAEgCADgDQACgEAAgFQAAgHgFgEQgGgEgIgCIgRgEQgKgCgIgDQgIgDgGgGQgFgGAAgLQAAgJAEgGQAEgGAGgEQAGgEAHgCQAIgBAHAAQAJAAAIABQAIACAHAEQAGAEAEAHQADAHABAKIgTAAQAAgFgCgEQgDgEgEgCQgDgCgFgBIgIgBIgIABIgIACQgEABgCAEQgCACAAAEQAAAGADACQAEAEAFACIAMADIAMAEIAOADIAMAFQAFADADAGQADAGAAAIQAAAKgEAHQgEAHgHAEQgHAEgJACIgRABQgJAAgIgBg");
	this.shape_8.setTransform(162.7,27.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#00338D").s().p("AgjBPIgGgBIAAgRIAFACIAFAAQAGAAAEgCQAEgDACgFIAIgTIgthxIAVAAIAgBdIABAAIAfhdIAUAAIgxCAIgGAOQgDAGgEAEQgDADgGACQgEACgGAAg");
	this.shape_9.setTransform(151.7,30.2);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#00338D").s().p("AAdA4QgFgFAAgJIgJAIQgEAEgFACQgJADgMAAQgHAAgIgBQgGgCgFgEQgFgDgEgHQgCgFAAgJQAAgKACgGQAEgGAFgEQAGgDAGgCIAPgDIAOgDIAMgCQAFgBADgDQADgCAAgGQAAgGgCgEQgDgEgDgCIgIgCIgIgBQgMAAgIAEQgIAFgBAMIgSAAQAAgKAFgHQADgHAHgFQAGgFAJgCQAJgBAJAAIAOAAQAIACAGADQAGAEAEAGQAEAGgBAKIAAA5IABAKQABADAFAAIAGgBIAAAPQgGACgIAAQgIAAgDgDgAANACIgLACIgMACIgMADQgFACgDAEQgDAFAAAGQAAAFACADIAFAFIAHADIAIABQAJAAAGgCQAGgDAFgEQAEgDACgGQACgEAAgEIAAgSQgEACgGABg");
	this.shape_10.setTransform(140.5,27.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#00338D").s().p("AgNBOQgGgBgEgCQgKgFgHgIQgGgIgEgLQgDgLAAgMQAAgMADgKQAEgLAGgIQAHgIAJgEQAKgFANAAIAJABIAKADQAFACAEADQAFAEADAFIAAAAIAAg7IATAAIAACcIgTAAIAAgPIAAAAQgFAJgKAFQgKAEgKAAgAgPgSQgHAEgEAGQgDAGgCAHQgCAIAAAIQAAAIACAIQACAHAEAGQAFAGAGAEQAHADAIAAQAKAAAGgDQAHgEAEgGQAEgGACgIIACgPQAAgJgCgIQgCgHgEgFQgFgGgGgEQgHgEgKAAQgIAAgHAEg");
	this.shape_11.setTransform(127.5,25.9);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#00338D").s().p("AgNBOQgGgBgEgCQgKgFgHgIQgGgIgEgLQgDgLAAgMQAAgMADgKQAEgLAGgIQAHgIAJgEQAKgFANAAIAJABIAKADQAFACAEADQAFAEADAFIAAAAIAAg7IATAAIAACcIgTAAIAAgPIAAAAQgFAJgKAFQgKAEgKAAgAgPgSQgHAEgEAGQgDAGgCAHQgCAIAAAIQAAAIACAIQACAHAEAGQAFAGAGAEQAHADAIAAQAKAAAGgDQAHgEAEgGQAEgGACgIIACgPQAAgJgCgIQgCgHgEgFQgFgGgGgEQgHgEgKAAQgIAAgHAEg");
	this.shape_12.setTransform(108.4,25.9);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#00338D").s().p("AgMA7IgLgEQgKgEgHgJQgHgHgEgMQgDgLAAgLQAAgMADgLQAEgLAHgIQAHgIAKgFQAKgEANAAQAOAAAKAEQAKAFAHAIQAHAIAEALQADALAAAMQAAALgDALQgEAMgHAHQgHAJgKAEIgLAEIgNAAgAgNgnQgGADgFAGQgFAFgDAIQgDAIAAAKQAAAJADAJQADAHAFAGQAFAFAGADQAHADAGAAQAHAAAHgDQAGgDAFgFQAFgGADgHQACgJAAgJQAAgKgCgIQgDgIgFgFQgFgGgGgDQgHgDgHAAQgGAAgHADg");
	this.shape_13.setTransform(96,27.9);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#00338D").s().p("AgMA7IgLgEQgKgEgHgJQgHgHgEgMQgDgLAAgLQAAgMADgLQAEgLAHgIQAHgIAKgFQAKgEANAAQAOAAAKAEQAKAFAHAIQAHAIAEALQADALAAAMQAAALgDALQgEAMgHAHQgHAJgKAEIgLAEIgNAAgAgNgnQgGADgFAGQgFAFgDAIQgDAIAAAKQAAAJADAJQADAHAFAGQAFAFAGADQAHADAGAAQAHAAAHgDQAGgDAFgFQAFgGADgHQACgJAAgJQAAgKgCgIQgDgIgFgFQgFgGgGgDQgHgDgHAAQgGAAgHADg");
	this.shape_14.setTransform(83.4,27.9);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#00338D").s().p("AgQBQQgIgCgGgEQgIgEgEgHQgEgGgBgJIATAAQABAFACADQADAEAEACIAKADIAIABQAJAAAHgDQAGgDAEgGQAFgFACgIQABgIAAgKIAAgHIAAAAQgFAKgKAFQgKAFgJAAQgNAAgJgEQgKgFgGgIQgGgHgEgLQgDgJAAgMQAAgKADgLQACgLAHgIQAFgJALgGQAKgFANAAQALAAAKAFQAIAEAFAKIAAgRIASAAIAABoQAAAOgDAKQgDAKgGAHQgNAOgbAAIgQgBgAgNg8QgGADgEAGQgFAGgBAHQgCAIAAAIIABAPQACAHADAGQAEAGAHAEQAFAEAJAAQAJAAAGgEQAHgEAEgGQAEgGACgHQABgIAAgIIgBgPQgCgHgEgGQgDgGgHgDQgGgEgJAAQgHAAgHAEg");
	this.shape_15.setTransform(70.5,30.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#00338D").s().p("AgNBOQgGgBgEgCQgKgFgHgIQgGgIgEgLQgDgLAAgMQAAgMADgKQAEgLAGgIQAHgIAJgEQAKgFANAAIAJABIAKADQAFACAEADQAFAEADAFIAAAAIAAg7IATAAIAACcIgTAAIAAgPIAAAAQgFAJgKAFQgKAEgKAAgAgPgSQgHAEgEAGQgDAGgCAHQgCAIAAAIQAAAIACAIQACAHAEAGQAFAGAGAEQAHADAIAAQAKAAAGgDQAHgEAEgGQAEgGACgIIACgPQAAgJgCgIQgCgHgEgFQgFgGgGgEQgHgEgKAAQgIAAgHAEg");
	this.shape_16.setTransform(51.5,25.9);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#00338D").s().p("AAcA6IAAhMQAAgFgBgEQgCgEgDgDQgDgEgEgBQgEgCgGAAQgHAAgHADQgGADgEAEQgEAFgCAHQgCAGAAAIIAAA/IgTAAIAAhxIARAAIAAATIABAAQAGgLAJgFQAJgFAMAAQALAAAIADQAHADAFAGQAEAFACAIQACAHAAAJIAABKg");
	this.shape_17.setTransform(39.3,27.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#00338D").s().p("AAxBOIgQgvIhCAAIgSAvIgVAAIA+ibIAVAAIA+CbgAgaANIA0AAIgahIIAAAAg");
	this.shape_18.setTransform(26.1,25.8);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#00338D").s().p("AgLAMIAAgXIAXAAIAAAXg");
	this.shape_19.setTransform(242.4,5.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#00338D").s().p("AgNBOQgGgBgEgCQgKgFgHgIQgGgIgEgLQgDgLAAgMQAAgMADgKQAEgLAGgIQAHgIAJgEQAKgFANAAIAJABIAKADQAFACAEADQAFAEADAFIAAAAIAAg7IATAAIAACcIgTAAIAAgPIAAAAQgFAJgKAFQgKAEgKAAgAgPgSQgHAEgEAGQgDAGgCAHQgCAIAAAIQAAAIACAIQACAHAEAGQAFAGAGAEQAHADAIAAQAKAAAGgDQAHgEAEgGQAEgGACgIIACgPQAAgJgCgIQgCgHgEgFQgFgGgGgEQgHgEgKAAQgIAAgHAEg");
	this.shape_20.setTransform(232.5,-0.8);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#00338D").s().p("AgMA7IgLgEQgKgEgHgJQgHgHgEgMQgDgLAAgMQAAgMADgKQAEgLAHgIQAHgIAKgFQAKgEANAAQAOAAAKAEQAKAFAHAIQAHAIAEALQADAKAAAMQAAAMgDALQgEAMgHAHQgHAJgKAEIgLAEIgNAAgAgNgnQgGADgFAGQgFAFgDAIQgDAIAAAJQAAALADAHQADAJAFAFQAFAGAGACQAHADAGAAQAHAAAHgDQAGgCAFgGQAFgFADgJQACgHAAgLQAAgJgCgIQgDgIgFgFQgFgGgGgDQgHgDgHAAQgGAAgHADg");
	this.shape_21.setTransform(220,1.3);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#00338D").s().p("AgMA7IgLgEQgKgEgHgJQgHgHgEgMQgDgLAAgMQAAgMADgKQAEgLAHgIQAHgIAKgFQAKgEANAAQAOAAAKAEQAKAFAHAIQAHAIAEALQADAKAAAMQAAAMgDALQgEAMgHAHQgHAJgKAEIgLAEIgNAAgAgNgnQgGADgFAGQgFAFgDAIQgDAIAAAJQAAALADAHQADAJAFAFQAFAGAGACQAHADAGAAQAHAAAHgDQAGgCAFgGQAFgFADgJQACgHAAgLQAAgJgCgIQgDgIgFgFQgFgGgGgDQgHgDgHAAQgGAAgHADg");
	this.shape_22.setTransform(207.4,1.3);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#00338D").s().p("AgQBQQgIgCgHgEQgGgEgFgHQgEgGgBgJIATAAQABAFACADQADAEAFACIAJADIAJABQAIAAAHgDQAGgDAFgGQAEgFABgIQACgIAAgKIAAgHIAAAAQgEAKgKAFQgKAFgKAAQgNAAgKgEQgJgFgHgIQgFgHgEgLQgDgJAAgMQAAgKACgLQADgLAGgIQAHgJAKgGQAKgFAOAAQAKAAAKAFQAJAEAEAKIAAgRIASAAIAABoQAAAOgDAKQgDAKgHAHQgMAOgbAAIgQgBgAgNg8QgHADgDAGQgEAGgDAHQgBAIgBAIIACAPQACAHAEAGQADAGAHAEQAFAEAJAAQAJAAAGgEQAHgEAEgGQAEgGACgHQABgIAAgIIgBgPQgCgHgDgGQgFgGgGgDQgGgEgIAAQgJAAgGAEg");
	this.shape_23.setTransform(194.5,3.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#00338D").s().p("AgRA6QgJgCgGgFQgHgFgEgHQgEgHAAgLIASAAQABAGACAFQADAEAEADQAEACAGABIAKABIAJgBIAJgCQAEgCADgDQACgDAAgGQAAgHgFgEQgGgEgIgCIgRgEQgKgCgIgDQgIgDgGgGQgFgGAAgLQAAgIAEgHQAEgGAGgEQAGgDAHgDQAIgBAHAAQAJAAAIABQAIACAHAEQAGAEAEAHQADAHABAKIgTAAQAAgGgCgDQgDgEgEgCQgDgCgFgBIgIgBIgIABIgIACQgEACgCACQgCADAAAEQAAAGADACQAEAEAFACIAMADIAMADIAOAEIAMAFQAFAEADAFQADAFAAAJQAAAKgEAHQgEAHgHAEQgHAEgJACIgRABQgJAAgIgBg");
	this.shape_24.setTransform(176.9,1.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#00338D").s().p("AgjBPIgGgBIAAgRIAGACIAEAAQAHAAADgCQAEgDACgFIAIgTIgthxIAVAAIAgBdIABAAIAghdIAUAAIgyCAIgGAOQgDAGgEAEQgEADgEACQgFACgGAAg");
	this.shape_25.setTransform(165.9,3.5);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#00338D").s().p("AAcA4QgEgFAAgJIgIAIQgFADgFADQgJAEgMgBQgIAAgGgBQgHgCgGgEQgEgEgEgGQgDgFAAgJQAAgKADgGQAEgGAFgEQAGgDAHgCIAOgDIAOgDIAMgCQAFgBADgDQADgCAAgGQAAgGgCgEQgCgDgEgDIgIgCIgIgBQgMAAgIAEQgIAFAAAMIgTAAQABgKAEgHQADgHAHgFQAHgEAIgDQAJgBAJAAIAPABQAHABAGADQAGAEAEAGQAEAGAAAKIAAA5IABAKQAAADAEAAIAHgBIAAAPQgFACgJAAQgIABgEgEgAANACIgLACIgMACIgMADQgEACgDAEQgEAFAAAGQAAAFACADIAFAFIAHADIAIABQAJAAAGgCQAGgDAFgEQAEgEACgFQACgEAAgEIAAgSQgDACgHABg");
	this.shape_26.setTransform(154.8,1.3);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#00338D").s().p("AgNBOQgGgBgEgCQgKgFgHgIQgGgIgEgLQgDgLAAgMQAAgMADgKQAEgLAGgIQAHgIAJgEQAKgFANAAIAJABIAKADQAFACAEADQAFAEADAFIAAAAIAAg7IATAAIAACcIgTAAIAAgPIAAAAQgFAJgKAFQgKAEgKAAgAgPgSQgHAEgEAGQgDAGgCAHQgCAIAAAIQAAAIACAIQACAHAEAGQAFAGAGAEQAHADAIAAQAKAAAGgDQAHgEAEgGQAEgGACgIIACgPQAAgJgCgIQgCgHgEgFQgFgGgGgEQgHgEgKAAQgIAAgHAEg");
	this.shape_27.setTransform(141.8,-0.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#00338D").s().p("AgNBOQgGgBgEgCQgKgFgHgIQgGgIgEgLQgDgLAAgMQAAgMADgKQAEgLAGgIQAHgIAJgEQAKgFANAAIAJABIAKADQAFACAEADQAFAEADAFIAAAAIAAg7IATAAIAACcIgTAAIAAgPIAAAAQgFAJgKAFQgKAEgKAAgAgPgSQgHAEgEAGQgDAGgCAHQgCAIAAAIQAAAIACAIQACAHAEAGQAFAGAGAEQAHADAIAAQAKAAAGgDQAHgEAEgGQAEgGACgIIACgPQAAgJgCgIQgCgHgEgFQgFgGgGgEQgHgEgKAAQgIAAgHAEg");
	this.shape_28.setTransform(122.7,-0.8);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#00338D").s().p("AAcA4QgEgFAAgJIgIAIQgFADgFADQgJAEgMgBQgIAAgGgBQgHgCgGgEQgEgEgDgGQgEgFAAgJQAAgKAEgGQADgGAGgEQAFgDAHgCIAOgDIAOgDIAMgCQAFgBADgDQADgCAAgGQAAgGgCgEQgCgDgEgDIgIgCIgIgBQgMAAgIAEQgIAFAAAMIgTAAQAAgKAEgHQAEgHAHgFQAGgEAJgDQAIgBAKAAIAPABQAHABAGADQAGAEAEAGQADAGABAKIAAA5IABAKQAAADAEAAIAGgBIAAAPQgEACgJAAQgHABgFgEgAAOACIgMACIgMACIgLADQgGACgCAEQgEAFAAAGQAAAFACADIAFAFIAHADIAIABQAKAAAFgCQAHgDAEgEQAEgEACgFQACgEAAgEIAAgSQgEACgFABg");
	this.shape_29.setTransform(110.9,1.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#00338D").s().p("AgGBPIgKgDIgKgFQgEgEgDgFIgBAAIAAAPIgTAAIAAicIATAAIAAA7IABAAQACgFADgDQAEgEAFgCQAKgEALAAIAMABQAGABAEADQAKAFAHAIQAGAIADALQAEAKAAAMQAAAMgDALQgEAKgGAIQgHAIgJAFQgKAFgNAAgAgQgSQgGAEgFAGQgEAGgCAHQgBAHAAAIQAAAJACAHQABAIAEAGQAFAGAHAEQAGADAJAAQAKAAAGgDQAHgEAEgGQAEgGABgIQACgIAAgJQAAgHgCgIQgCgHgEgGQgEgGgHgDQgHgEgIAAQgJAAgHAEg");
	this.shape_30.setTransform(98.6,-0.8);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#00338D").s().p("AgQBQQgIgCgGgEQgIgEgEgHQgEgGgBgJIATAAQABAFACADQADAEAEACIAKADIAIABQAJAAAHgDQAGgDAEgGQAFgFACgIQABgIAAgKIAAgHIAAAAQgFAKgKAFQgKAFgJAAQgNAAgJgEQgKgFgGgIQgGgHgEgLQgDgJAAgMQAAgKADgLQACgLAHgIQAFgJALgGQAKgFANAAQALAAAKAFQAIAEAFAKIAAgRIASAAIAABoQAAAOgDAKQgDAKgGAHQgNAOgbAAIgQgBgAgNg8QgGADgEAGQgFAGgBAHQgCAIAAAIIABAPQACAHADAGQAEAGAHAEQAFAEAJAAQAJAAAGgEQAHgEAEgGQAEgGACgHQABgIAAgIIgBgPQgCgHgEgGQgDgGgHgDQgGgEgJAAQgHAAgHAEg");
	this.shape_31.setTransform(79,3.4);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#00338D").s().p("AAcA6IAAhMQAAgFgBgEQgCgEgDgDQgDgEgEgBQgEgCgGAAQgHAAgHADQgGADgEAEQgEAFgCAHQgCAGAAAIIAAA/IgTAAIAAhxIARAAIAAATIABAAQAGgLAJgFQAJgFAMAAQALAAAIADQAHADAFAGQAEAFACAIQACAHAAAJIAABKg");
	this.shape_32.setTransform(66.9,1.1);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#00338D").s().p("AgJBOIAAhwIASAAIAABwgAgJg3IAAgWIASAAIAAAWg");
	this.shape_33.setTransform(58.4,-0.9);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#00338D").s().p("AAaBOIgmg7IgRAQIAAArIgTAAIAAibIATAAIAABbIAzgwIAYAAIgtAoIAxBIg");
	this.shape_34.setTransform(51,-0.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#00338D").s().p("AAdA4QgFgFAAgJIgJAIQgEADgFADQgJAEgMgBQgHAAgIgBQgHgCgFgEQgEgEgEgGQgCgFAAgJQAAgKACgGQAEgGAFgEQAGgDAGgCIAPgDIAOgDIAMgCQAFgBADgDQADgCAAgGQAAgGgCgEQgDgDgDgDIgIgCIgIgBQgMAAgIAEQgIAFgBAMIgSAAQAAgKAFgHQADgHAHgFQAGgEAJgDQAIgBAKAAIAOABQAIABAGADQAGAEAEAGQAEAGgBAKIAAA5IABAKQABADAEAAIAHgBIAAAPQgGACgIAAQgIABgDgEgAANACIgLACIgMACIgMADQgEACgEAEQgDAFAAAGQAAAFACADIAFAFIAHADIAIABQAKAAAFgCQAHgDAEgEQAEgEACgFQACgEAAgEIAAgSQgDACgHABg");
	this.shape_35.setTransform(38.9,1.3);

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#00338D").s().p("AA7BOIAAiBIgBAAIgxCBIgRAAIgxiBIAAAAIAACBIgUAAIAAibIAcAAIAxCCIAyiCIAcAAIAACbg");
	this.shape_36.setTransform(23.2,-0.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_36},{t:this.shape_35},{t:this.shape_34},{t:this.shape_33},{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txtslogan, new cjs.Rectangle(0,-16,259,56.3), null);


(lib.txtroadside = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMANIAAgZIAZAAIAAAZg");
	this.shape.setTransform(136.8,54.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgWA6QgKgFgHgJQgHgIgEgLQgDgMgBgNQAAgNAEgLQAEgLAIgJQAHgIALgEQAKgFALAAQAIAAAGACQAHABAFADQALAHAGAKQAGAKADAMQACAMAAAKIhYAAQAAAIACAHQACAHAEAFQAFAGAHADQAHADAJAAQAMAAAIgFQAEgEADgEIAEgKIATAAQgEAUgNAKQgHAFgIADQgJACgKAAQgNAAgLgEgAgNgpQgGADgEAEQgFAEgCAHQgDAGAAAGIBDAAQAAgGgDgGQgDgHgEgEQgFgEgGgDQgGgDgHAAQgHAAgGADg");
	this.shape_1.setTransform(127.6,49.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOBSQgGgBgFgDQgKgFgHgIQgHgJgDgLQgDgLAAgNQAAgNADgKQADgLAHgJQAHgIAKgFQAKgFAOAAIAJABIAKADQAGACAEAEQAFADADAGIABAAIAAg9IATAAIAACjIgTAAIAAgQIgBAAQgFAKgKAEQgKAEgMAAgAgQgTQgHAEgEAHQgEAGgCAHQgCAJAAAIQAAAJACAIQADAHAEAHQAFAGAHAEQAHADAIAAQAKAAAHgEQAHgEAEgGQAEgGACgIQACgIAAgJQAAgIgCgJQgCgHgEgGQgFgGgHgEQgHgEgKAAQgJAAgHAEg");
	this.shape_2.setTransform(114.2,47.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgJBSIAAh2IATAAIAAB2gAgJg5IAAgYIATAAIAAAYg");
	this.shape_3.setTransform(105.2,47.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgSA8QgJgCgGgFQgIgFgEgHQgEgIgBgLIAUAAQABAHACAEQADAFAEACQAFADAFABIALABIAKgBIAJgCQAFgCACgEQADgDAAgGQAAgHgGgEQgGgEgIgDIgSgEQgKgCgIgDQgJgDgGgHQgGgGAAgMQAAgJAEgGQAEgGAHgFQAGgEAIgBQAIgCAHAAQAJAAAJACQAIABAHAEQAHAFADAHQAEAIACAKIgVAAQAAgFgCgFIgHgFQgDgDgGgBIgIgBIgJAAIgIADIgGAFQgCADAAAEQAAAGAEADQADADAGACIAMAEIANADIAOAEQAHACAGADQAFAEAEAFQACAHAAAIQABAKgFAIQgEAHgIAEQgHAEgJACIgSACQgJAAgJgCg");
	this.shape_4.setTransform(96.9,49.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgOBSQgGgBgFgDQgKgFgHgIQgHgJgDgLQgDgLAAgNQAAgNADgKQADgLAHgJQAHgIAKgFQAKgFAOAAIAJABIAKADQAGACAEAEQAFADADAGIABAAIAAg9IATAAIAACjIgTAAIAAgQIgBAAQgFAKgKAEQgKAEgMAAgAgQgTQgHAEgEAHQgEAGgCAHQgCAJAAAIQAAAJACAIQADAHAEAHQAFAGAHAEQAHADAIAAQAKAAAHgEQAHgEAEgGQAEgGACgIQACgIAAgJQAAgIgCgJQgCgHgEgGQgFgGgHgEQgHgEgKAAQgJAAgHAEg");
	this.shape_5.setTransform(84,47.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAeA6QgFgFAAgJIgJAIIgJAGQgKAEgNAAQgIAAgHgCQgHgBgFgFQgGgEgDgGQgDgGAAgJQAAgKADgGQAEgHAGgEQAFgDAHgCIAPgEIAPgCIAMgCQAGgCADgDQADgDAAgFQAAgGgCgFQgDgDgDgCIgJgEIgIAAQgNAAgIAFQgIAFgBANIgTAAQAAgLAEgIQAEgHAHgGQAHgEAJgCQAJgCAKAAIAPABQAIABAGADQAHAEAEAGQAEAHAAAKIAAA9IAAAKQABADAFAAIAGgBIAAAPQgFADgJAAQgIAAgEgEgAAOACIgMACIgMACQgHABgFACQgGACgDAFQgDAEAAAHQAAAFACADQACAEADACIAHADIAJABQAJAAAGgDQAHgCAFgFIAGgIQACgFAAgEIAAgTQgEACgGABg");
	this.shape_6.setTransform(71.6,49.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgMA9IgMgDQgLgGgHgIQgIgIgDgLQgEgMAAgNQAAgMAEgLQADgLAIgJQAHgJALgEQALgFANAAQAOAAALAFQAKAEAIAJQAHAJAEALQAEALAAAMQAAANgEAMQgEALgHAIQgIAIgKAGIgMADIgNABgAgOgpQgGADgFAGQgGAGgCAIQgDAIAAAKQAAALADAIQACAJAGAFQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgFADgJQADgIAAgLQAAgKgDgIQgDgIgFgGQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_7.setTransform(58.6,49.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAqBSQgCgDgBgEIgBgJIAAgNIgCgLIgCgMQgBgFgDgEQgEgFgFgBQgEgDgIAAIg0AAIAABGIgXAAIAAijIBNAAQALAAAJACQAKAEAHAFQAGAGADAIQADAIAAAKIgBAOIgFAMQgHALgPAEIAAAAQAHABAFADQAEADADAFIAEAKIACALIAAANIABAMIADALQACAGADAEgAgrgFIAsAAIAOgCQAHgBAGgDQAEgDAEgGQADgGAAgJQAAgMgHgIQgEgEgFgCQgGgCgHAAIg1AAg");
	this.shape_8.setTransform(44.7,47.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgeA9IAAh2IASAAIAAAZIABAAQADgHAFgGQADgFAFgDQALgHAPAAIAAAVQgLAAgJADQgIADgEAGQgFAHgCAJQgDAIAAALIAAA1g");
	this.shape_9.setTransform(26.7,49.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgMA9IgMgDQgLgGgHgIQgIgIgDgLQgEgMAAgNQAAgMAEgLQADgLAIgJQAHgJALgEQALgFANAAQAOAAALAFQAKAEAIAJQAHAJAEALQAEALAAAMQAAANgEAMQgEALgHAIQgIAIgKAGIgMADIgNABgAgOgpQgGADgFAGQgGAGgCAIQgDAIAAAKQAAALADAIQACAJAGAFQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgFADgJQADgIAAgLQAAgKgDgIQgDgIgFgGQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_10.setTransform(15.6,49.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgMBTIAAhlIgUAAIAAgRIAUAAIAAgSQAAgHACgFQACgGAEgDQAEgFAGgBQAGgCAIAAIAGAAIAHACIAAARIgGgBIgFgBQgIAAgDADQgEADAAAHIAAARIAXAAIAAARIgXAAIAABlg");
	this.shape_11.setTransform(5.8,47.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AA0BSIgSgyIhEAAIgTAyIgWAAIBAijIAXAAIBACjgAgbAOIA2AAIgbhMIAAAAg");
	this.shape_12.setTransform(141.7,19.7);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AA0BSIgSgyIhEAAIgTAyIgWAAIBAijIAXAAIBACjgAgbAOIA2AAIgbhMIAAAAg");
	this.shape_13.setTransform(126.8,19.7);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgeBPQgHgDgGgEQgGgFgFgGQgJgLgFgQQgEgPgBgSQAAgRAGgQIAFgPIAJgNQAKgLAOgHQAIgEAJgBQAHgCAJAAQANAAAMAEQALADAJAHQAJAGAGAKQAGAKABANIgVAAQgEgRgNgJQgFgEgIgCQgHgCgJAAQgOAAgKAFQgKAGgIAJQgGAJgEAMQgCAMAAANQAAANACANQAEAMAGAJQAIAKAKAFQAKAGAOAAQALAAAJgEQAIgDAGgHQAGgGAEgJQADgJAAgKIAXAAQgEAfgSASQgTARgeAAQgTAAgOgHg");
	this.shape_14.setTransform(111.1,19.7);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAYA7IgYhdIAAAAIgXBdIgVAAIgmh1IAVAAIAbBgIABAAIAYhgIAUAAIAZBgIAAAAIAbhgIAUAAIgmB1g");
	this.shape_15.setTransform(87.7,22);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgMA9IgMgDQgLgGgHgIQgIgIgDgMQgEgLAAgNQAAgMAEgLQADgLAIgJQAHgJALgFQALgEANAAQAOAAALAEQAKAFAIAJQAHAJAEALQAEALAAAMQAAANgEALQgEAMgHAIQgIAIgKAGIgMADIgNABgAgOgpQgGADgFAGQgGAGgCAIQgDAIAAAKQAAALADAJQACAIAGAFQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgFADgIQADgJAAgLQAAgKgDgIQgDgIgFgGQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_16.setTransform(72.4,22);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AAeA9IAAhQQAAgFgCgFQgBgEgEgDQgDgDgEgCQgFgCgFAAQgIAAgHADQgGADgFAFQgEAFgCAHQgCAHAAAIIAABCIgUAAIAAh2IASAAIAAATIABAAQAGgLAKgFQAJgGAMAAQAMAAAIADQAIAEAFAFQAFAGABAIQACAIAAAKIAABNg");
	this.shape_17.setTransform(59.4,21.8);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAoBSIg6hSIgbAYIAAA6IgWAAIAAijIAWAAIAABRIBShRIAcAAIhEBCIBHBhg");
	this.shape_18.setTransform(46.3,19.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgbA6QgHgDgFgGQgFgGgCgIQgCgIAAgJIAAhOIAUAAIAABQQAAAFABAFQACAEADADQAGAHAMAAQAIAAAGgDQAHgDAEgFQAFgFACgHQACgGAAgJIAAhCIATAAIAAB2IgSAAIAAgTIAAAAQgHALgJAGQgKAFgMAAQgMAAgIgDg");
	this.shape_19.setTransform(25,22.1);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgMA9IgMgDQgLgGgHgIQgIgIgDgMQgEgLAAgNQAAgMAEgLQADgLAIgJQAHgJALgFQALgEANAAQAOAAALAEQAKAFAIAJQAHAJAEALQAEALAAAMQAAANgEALQgEAMgHAIQgIAIgKAGIgMADIgNABgAgOgpQgGADgFAGQgGAGgCAIQgDAIAAAKQAAALADAJQACAIAGAFQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgFADgIQADgJAAgLQAAgKgDgIQgDgIgFgGQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_20.setTransform(12,22);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgJBSIAAhDIhAhgIAaAAIAwBOIAxhOIAYAAIg/BgIAABDg");
	this.shape_21.setTransform(0.5,19.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txtroadside, new cjs.Rectangle(-23.8,4,189.8,58.6), null);


(lib.txtpoolside = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMBUIAAgZIAZAAIAAAZgAgIAsIAAgLQAAgJADgIQADgGAEgFIAJgJIAJgJQAFgGACgGQADgFgBgJQgBgNgHgGQgEgFgFgBQgEgCgHAAQgHAAgGADQgGADgEAFQgEAFgCAHQgCAHAAAIIgTAAQAAgLADgLQAEgKAGgHQAHgIAKgEQAJgEALAAQALAAAJADQAJADAGAGQAHAGADAJQAEAIAAALQAAAHgCAFQgBAGgDAFIgHAJIgHAGIgHAHIgGAHIgFAKQgCAEAAAGIAAAJg");
	this.shape.setTransform(188.6,49.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgWA6QgKgFgHgJQgHgHgEgMQgDgMgBgNQAAgNAEgLQAEgMAIgHQAHgJALgFQAKgEALAAQAIAAAGACQAHABAFADQALAHAGAKQAGAKADAMQACAMAAAKIhYAAQAAAIACAHQACAGAEAGQAFAFAHAEQAHADAJAAQAMAAAIgFQAEgDADgFIAEgKIATAAQgEAUgNAKQgHAFgIACQgJADgKAAQgNAAgLgEgAgNgpQgGADgEAEQgFAEgCAHQgDAGAAAGIBDAAQAAgGgDgGQgDgHgEgEQgFgEgGgDQgGgDgHAAQgHAAgGADg");
	this.shape_1.setTransform(176.3,51.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOBSQgGgBgFgDQgKgFgHgIQgHgJgDgLQgDgLAAgNQAAgNADgKQADgLAHgJQAHgIAKgFQAKgFAOAAIAJABIAKADQAGACAEAEQAFADADAGIABAAIAAg9IATAAIAACjIgTAAIAAgQIgBAAQgFAKgKAEQgKAEgMAAgAgQgTQgHAEgEAHQgEAGgCAHQgCAJAAAIQAAAJACAIQADAHAEAHQAFAGAHAEQAHADAIAAQAKAAAHgEQAHgEAEgGQAEgGACgIQACgIAAgJQAAgIgCgJQgCgHgEgGQgFgGgHgEQgHgEgKAAQgJAAgHAEg");
	this.shape_2.setTransform(162.9,49.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgJBSIAAh2IATAAIAAB2gAgJg6IAAgXIATAAIAAAXg");
	this.shape_3.setTransform(153.9,49.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgSA8QgJgCgGgFQgIgEgEgJQgEgHgBgLIAUAAQABAGACAFQADAEAEADQAFADAGABIAKABIAKgBIAJgCQAFgCACgDQADgEAAgFQAAgJgGgDQgGgEgIgCIgSgFQgKgCgIgDQgKgDgFgHQgGgGAAgMQAAgIAEgHQAEgGAGgFQAHgEAIgBQAIgCAHAAQAJAAAJACQAJABAGAEQAHAFADAHQAFAHABALIgUAAQAAgFgDgEIgGgGQgFgDgEgBIgJgBIgIABIgJACIgGAFQgCACAAAFQAAAFADADQAEAEAFACIANAEIAMADIAPAEQAHACAFADQAGAEAEAGQACAFAAAJQABALgFAGQgFAIgGAEQgIAEgJACIgSACQgJAAgJgCg");
	this.shape_4.setTransform(145.6,51.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgJBSIAAijIATAAIAACjg");
	this.shape_5.setTransform(137.3,49.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgMA9IgMgDQgLgFgHgJQgIgIgDgMQgEgLAAgNQAAgMAEgLQADgLAIgJQAHgJALgFQALgEANAAQAOAAALAEQAKAFAIAJQAHAJAEALQAEALAAAMQAAANgEALQgEAMgHAIQgIAJgKAFIgMADIgNABgAgOgpQgGADgFAGQgGAGgCAIQgDAJAAAJQAAALADAJQACAHAGAGQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgGADgHQADgJAAgLQAAgJgDgJQgDgIgFgGQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_6.setTransform(128.1,51.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgMA9IgMgDQgLgFgHgJQgIgIgDgMQgEgLAAgNQAAgMAEgLQADgLAIgJQAHgJALgFQALgEANAAQAOAAALAEQAKAFAIAJQAHAJAEALQAEALAAAMQAAANgEALQgEAMgHAIQgIAJgKAFIgMADIgNABgAgOgpQgGADgFAGQgGAGgCAIQgDAJAAAJQAAALADAJQACAHAGAGQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgGADgHQADgJAAgLQAAgJgDgJQgDgIgFgGQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_7.setTransform(114.9,51.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("Ag8BSIAAijIBHAAQAZAAAMANQAHAGADAJQADAJAAAMQAAAWgNAMQgMANgZAAIgxAAIAABDgAgmgDIApAAQATAAAJgHQAIgIAAgOQAAgQgIgHQgJgIgTAAIgpAAg");
	this.shape_8.setTransform(101.3,49.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgSA8QgJgCgHgFQgGgEgFgJQgEgHgBgLIAUAAQABAGACAFQADAEAFADQAEADAGABIAKABIAKgBIAJgCQAEgCADgDQADgEAAgFQAAgJgGgDQgFgEgJgCIgSgFQgKgCgIgDQgKgDgFgHQgGgGAAgMQAAgIAEgHQAEgGAGgFQAHgEAIgBQAIgCAHAAQAJAAAJACQAJABAGAEQAHAFADAHQAFAHABALIgUAAQAAgFgDgEIgGgGQgFgDgEgBIgJgBIgIABIgJACIgGAFQgCACAAAFQAAAFADADQAEAEAFACIANAEIAMADIAPAEQAHACAFADQAGAEADAGQADAFAAAJQABALgFAGQgFAIgGAEQgIAEgJACIgSACQgJAAgJgCg");
	this.shape_9.setTransform(81.3,51.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgWA6QgKgFgHgJQgHgHgEgMQgDgMgBgNQAAgNAEgLQAEgMAIgHQAHgJALgFQAKgEALAAQAIAAAGACQAHABAFADQALAHAGAKQAGAKADAMQACAMAAAKIhYAAQAAAIACAHQACAGAEAGQAFAFAHAEQAHADAJAAQAMAAAIgFQAEgDADgFIAEgKIATAAQgEAUgNAKQgHAFgIACQgJADgKAAQgNAAgLgEgAgNgpQgGADgEAEQgFAEgCAHQgDAGAAAGIBDAAQAAgGgDgGQgDgHgEgEQgFgEgGgDQgGgDgHAAQgHAAgGADg");
	this.shape_10.setTransform(69.5,51.8);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AgMA9IgMgDQgLgFgHgJQgIgIgDgMQgEgLAAgNQAAgMAEgLQADgLAIgJQAHgJALgFQALgEANAAQAOAAALAEQAKAFAIAJQAHAJAEALQAEALAAAMQAAANgEALQgEAMgHAIQgIAJgKAFIgMADIgNABgAgOgpQgGADgFAGQgGAGgCAIQgDAJAAAJQAAALADAJQACAHAGAGQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgGADgHQADgJAAgLQAAgJgDgJQgDgIgFgGQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_11.setTransform(56.6,51.8);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AhDBSIAAijIA5AAQARAAAPAFQAPAEAKAKQAKAKAGAOQAEAPAAAUQAAATgEAQQgCAJgEAHIgIALQgKAMgPAFQgPAGgTAAgAgsBAIAkAAIAIgBQAGAAAGgDQAGgCAGgEQAGgEAFgIQAEgGAEgLQADgLAAgOQAAgPgDgMQgDgLgHgIQgGgJgLgDQgJgFgPAAIgkAAg");
	this.shape_12.setTransform(42.4,49.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgMA9IgMgEQgLgEgHgJQgIgIgDgMQgEgLAAgNQAAgMAEgLQADgLAIgJQAHgIALgGQALgEANAAQAOAAALAEQAKAGAIAIQAHAJAEALQAEALAAAMQAAANgEALQgEAMgHAIQgIAJgKAEIgMAEIgNABgAgOgpQgGADgFAGQgGAFgCAJQgDAJAAAJQAAALADAJQACAHAGAGQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgGADgHQADgJAAgLQAAgJgDgJQgDgJgFgFQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_13.setTransform(232.1,24);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgSA8QgJgCgGgFQgIgEgEgJQgEgHgBgLIAUAAQABAGACAFQADAEAEADQAFADAFABIALABIAKAAIAJgDQAFgCACgDQADgEAAgFQAAgJgGgDQgGgEgIgCIgSgFQgKgCgIgDQgJgDgGgHQgGgGAAgMQAAgIAEgHQAEgGAHgEQAGgEAIgCQAIgCAHAAQAJAAAJACQAIABAHAFQAHAEADAHQAEAIACAKIgVAAQAAgFgCgEIgHgHQgDgCgGgBIgIgBIgJABIgIACIgGAFQgCACAAAFQAAAFADADQAEAEAGACIAMAEIANADIAOAEQAHACAGADQAFAEAEAGQACAFAAAJQABALgFAGQgEAIgIAEQgHAEgJACIgSACQgJAAgJgCg");
	this.shape_14.setTransform(219.7,24);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AgJBSIAAijIATAAIAACjg");
	this.shape_15.setTransform(211.4,21.7);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AA0BSIgSgyIhEAAIgTAyIgWAAIBAijIAXAAIBACjgAgbAOIA2AAIgbhLIAAAAg");
	this.shape_16.setTransform(201.4,21.7);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AA0BSIgSgyIhEAAIgTAyIgWAAIBAijIAXAAIBACjgAgbAOIA2AAIgbhLIAAAAg");
	this.shape_17.setTransform(180.2,21.7);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AA0BSIgSgyIhEAAIgTAyIgWAAIBAijIAXAAIBACjgAgbAOIA2AAIgbhLIAAAAg");
	this.shape_18.setTransform(165.3,21.7);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgeBPQgHgDgGgEQgGgFgFgGQgJgLgFgQQgEgPgBgSQAAgRAGgQIAFgPIAJgNQAKgLAPgHQAHgEAJgBQAHgCAJAAQANAAALAEQAMADAJAHQAJAGAGAKQAGAKABANIgVAAQgEgRgMgJQgHgEgHgCQgIgCgIAAQgOAAgKAFQgLAGgGAJQgIAJgCAMQgDAMgBANQABANADANQACAMAIAJQAHAKAKAFQAKAGAOAAQALAAAJgEQAHgDAHgHQAGgGADgJQAEgJABgKIAVAAQgDAfgSASQgSARggAAQgSAAgOgHg");
	this.shape_19.setTransform(149.6,21.7);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AAYA7IgYhdIAAAAIgXBdIgVAAIgmh1IAVAAIAbBgIABAAIAYhgIAUAAIAZBgIAAAAIAbhgIAUAAIgmB1g");
	this.shape_20.setTransform(126.2,24);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AgMA9IgMgEQgLgEgHgJQgIgIgDgMQgEgLAAgNQAAgMAEgLQADgLAIgJQAHgIALgGQALgEANAAQAOAAALAEQAKAGAIAIQAHAJAEALQAEALAAAMQAAANgEALQgEAMgHAIQgIAJgKAEIgMAEIgNABgAgOgpQgGADgFAGQgGAFgCAJQgDAJAAAJQAAALADAJQACAHAGAGQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgGADgHQADgJAAgLQAAgJgDgJQgDgJgFgFQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_21.setTransform(110.9,24);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAeA9IAAhQQAAgFgCgFQgBgEgEgDQgDgDgEgCQgFgCgFAAQgIAAgHADQgGADgFAFQgEAFgCAHQgCAHAAAIIAABCIgUAAIAAh2IASAAIAAATIABAAQAGgLAKgFQAJgGAMAAQAMAAAIADQAIAEAFAFQAFAGABAIQACAIAAAKIAABNg");
	this.shape_22.setTransform(97.9,23.8);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAoBSIg6hSIgbAYIAAA6IgWAAIAAijIAWAAIAABRIBShRIAcAAIhEBCIBHBhg");
	this.shape_23.setTransform(84.8,21.7);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AgbA6QgHgDgFgGQgFgGgCgIQgCgIAAgJIAAhOIAUAAIAABQQAAAFABAFQACAEADADQAGAHAMAAQAIAAAGgDQAHgDAEgFQAFgFACgHQACgGAAgJIAAhCIATAAIAAB2IgSAAIAAgTIAAAAQgHALgJAGQgKAFgMAAQgMAAgIgDg");
	this.shape_24.setTransform(63.5,24.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgMA9IgMgEQgLgEgHgJQgIgIgDgMQgEgLAAgNQAAgMAEgLQADgLAIgJQAHgIALgGQALgEANAAQAOAAALAEQAKAGAIAIQAHAJAEALQAEALAAAMQAAANgEALQgEAMgHAIQgIAJgKAEIgMAEIgNABgAgOgpQgGADgFAGQgGAFgCAJQgDAJAAAJQAAALADAJQACAHAGAGQAFAGAGADQAHADAHAAQAHAAAHgDQAHgDAFgGQAFgGADgHQADgJAAgLQAAgJgDgJQgDgJgFgFQgFgGgHgDQgHgDgHAAQgHAAgHADg");
	this.shape_25.setTransform(50.5,24);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgJBSIAAhDIhAhgIAaAAIAwBOIAwhOIAZAAIg/BgIAABDg");
	this.shape_26.setTransform(39,21.7);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AgOBSQgGgBgFgDQgKgFgHgIQgHgJgDgLQgDgLAAgNQAAgNADgKQADgLAHgJQAHgIAKgFQAKgFAOAAIAJABIAKADQAGACAEAEQAFADADAGIABAAIAAg9IATAAIAACjIgTAAIAAgQIgBAAQgFAKgKAEQgKAEgMAAgAgQgTQgHAEgEAHQgEAGgCAHQgCAJAAAIQAAAJACAIQADAHAEAHQAFAGAHAEQAHADAIAAQAKAAAHgEQAHgEAEgGQAEgGACgIQACgIAAgJQAAgIgCgJQgCgHgEgGQgFgGgHgEQgHgEgKAAQgJAAgHAEg");
	this.shape_27.setTransform(18.1,21.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AgJBSIAAh2IATAAIAAB2gAgJg6IAAgXIATAAIAAAXg");
	this.shape_28.setTransform(9.1,21.7);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AhCBSIAAijIA4AAQASAAAPAEQAOAFAKAKQALAKAEAOQAGAPAAAUQgBATgEAQQgCAIgEAIIgIALQgKAMgPAFQgPAGgTAAgAgtBAIAkAAIAJgBQAFgBAHgCQAGgCAGgEQAGgEAFgIQAEgGAEgLQADgLAAgOQAAgPgDgLQgDgMgGgIQgHgJgLgDQgKgFgOAAIglAAg");
	this.shape_29.setTransform(-1.1,21.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.txtpoolside, new cjs.Rectangle(-13,6,255,58.6), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = getMCSymbolPrototype(lib.Symbol1, null, null);


(lib.family = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

}).prototype = getMCSymbolPrototype(lib.family, null, null);


// stage content:
(lib._300x250__displayad = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_182 = function() {
		timesPlayed++;
		
		if (timesPlayed == 2) {
			this.stop();
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(182).call(this.frame_182).wait(34));

	// Layer 6
	this.instance = new lib.burger_0000_buntop();
	this.instance.parent = this;
	this.instance.setTransform(93,7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(216));

	// Layer 5
	this.instance_1 = new lib.burger_0001_lettuce();
	this.instance_1.parent = this;
	this.instance_1.setTransform(93,39);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(216));

	// Layer 4
	this.instance_2 = new lib.burger_0002_cucumbers();
	this.instance_2.parent = this;
	this.instance_2.setTransform(117,80);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(216));

	// Layer 3
	this.instance_3 = new lib.burger_0003_bacon();
	this.instance_3.parent = this;
	this.instance_3.setTransform(93,88);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(216));

	// Layer 2
	this.instance_4 = new lib.burger_0004_beef();
	this.instance_4.parent = this;
	this.instance_4.setTransform(85,105);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(216));

	// Layer 1
	this.instance_5 = new lib.burger_0005_bunbottom();
	this.instance_5.parent = this;
	this.instance_5.setTransform(96,146);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(216));

	// logo
	this.instance_6 = new lib.insurancelogo();
	this.instance_6.parent = this;
	this.instance_6.setTransform(17,201);

	this.instance_7 = new lib.Bitmap3();
	this.instance_7.parent = this;
	this.instance_7.setTransform(167,204);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7},{t:this.instance_6}]}).wait(216));

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(1,1,1,3,true).p("AXXAAMgutAAA");
	this.shape.setTransform(150,193.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#EBEBEB").ss(1,1,1,3,true).p("AXczXIgKAAA3bzXIAKAAIAAAKAXSTYIAKAA");
	this.shape_1.setTransform(151,125);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(216));

	// txt good better
	this.instance_8 = new lib.txtslogan();
	this.instance_8.parent = this;
	this.instance_8.setTransform(152.5,115.3,1,1,0,0,0,129.5,26.3);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(129).to({_off:false},0).wait(1).to({regX:133.6,regY:14.7,x:156.6,y:103.7},0).wait(20).to({alpha:0.2},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:1},0).wait(56).to({alpha:0.833},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0},0).wait(1));

	// txt roadside
	this.instance_9 = new lib.txtroadside();
	this.instance_9.parent = this;
	this.instance_9.setTransform(150,140.4,1,1,0,0,0,71,26.3);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1).to({regX:71.2,regY:33.5,x:150.2,y:147.6},0).wait(9).to({alpha:0.2},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:1},0).wait(55).to({regX:71,regY:26.3,x:150,y:140.4},0).wait(1).to({regX:71.2,regY:33.5,x:150.2,y:147.6,alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(141));

	// txt poolside
	this.instance_10 = new lib.txtpoolside();
	this.instance_10.parent = this;
	this.instance_10.setTransform(108.5,140.4,1,1,0,0,0,71,26.3);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(68).to({_off:false},0).wait(1).to({regX:115,regY:35.5,x:152.5,y:149.6},0).wait(16).to({alpha:0.2},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:1},0).wait(56).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(66));

	// man
	this.instance_11 = new lib.Symbol1();
	this.instance_11.parent = this;
	this.instance_11.setTransform(151,97.5,1,1,0,0,0,150,96.5);
	this.instance_11.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(1).to({regX:0,regY:0,x:1,y:1,alpha:0.25},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:1},0).wait(172).to({_off:true},1).wait(1).to({_off:false,regX:150,regY:96.5,x:151,y:97.5},0).wait(1).to({regX:0,regY:0,x:1,y:1,alpha:0.833},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(31));

	// family
	this.instance_12 = new lib.family();
	this.instance_12.parent = this;
	this.instance_12.setTransform(150.1,97.5,1,1,0,0,0,150,96.5);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(58).to({_off:false},0).wait(79).to({regX:0,regY:0,x:0.1,y:1},0).wait(9).to({alpha:0.833},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0},0).wait(3).to({_off:true},1).wait(61));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(149.5,125,302.5,250.1);
// library properties:
lib.properties = {
	width: 300,
	height: 250,
	fps: 15,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"images/300x250__display_ad_atlas_.png", id:"300x250__display_ad_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;